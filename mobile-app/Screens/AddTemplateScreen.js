import React, { Component } from 'react';
import { Provider, connect } from 'react-redux';
import { FlatList, StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import { Card, ListItem, Button, CheckBox, Input } from 'react-native-elements'
import { actionCreator } from './../reducers/mainreducer.js';

const mapStateToProps = (state) => ({
  templates: state.templates,
})

class AddTemplateScreen extends Component {
    static navigationOptions = {
        title: "Add Template..",
    }
    newTemplateInput = ""
    saveInputToState = (string) => { this.newTemplateInput = string }
    
    saveNewTemplate = () => { 
      const {dispatch} = this.props
      dispatch(actionCreator.addTemplate(this.newTemplateInput))
      this.props.navigation.goBack(null)
    }

    render() {
        return (
            <View>
                <Input 
                    placeholder="Template name"
                    onChangeText={this.saveInputToState}
                />
                <Button
                    title="Save"
                    onPress={this.saveNewTemplate}
                />
            </View>
        )
    }
}

export default connect(mapStateToProps)(AddTemplateScreen)
