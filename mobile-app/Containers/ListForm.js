import React, { Component } from 'react';
import { Provider, connect } from 'react-redux';
import { FlatList, StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import { Card, ListItem, Button, CheckBox, Input, Divider } from 'react-native-elements'
import { actionCreator } from './../reducers/mainreducer.js';

const mapStateToProps = (state) => ({
  templates: state.templates,
})

class ListForm extends Component {
    constructor(props){
        super(props);
        this.state = { 
            name:"",
            items:[],
            templates:[],
        }
    }

    newItem = {
        id:Math.floor(Math.random() * 1000),
        name:"",
        isDone: false,
    }

    onChangeText = (changedName) => {this.setState({...this.state, name:changedName})}
    
    saveNewList = () => { 
      const {dispatch} = this.props
      const newList = {
            id: Math.floor(Math.random() * 1000),
            name:this.state.name,
            items:this.state.items
      }
      dispatch(actionCreator.addList(newList))
      this.props.navigation.goBack(null)
    }

    onCheckTemplate = (templateId) => {
        const {templates} = this.props;
        if(!this.isTemplateChecked(templateId)){
            templates.forEach(template => {
                if(template.id === templateId){
                    let newState = {...this.state, 
                                    items:[...this.state.items, ...template.items],
                                    templates:[...this.state.templates, templateId]
                    }
                    this.setState(newState)
                    return;
                }
            });
        }
        else{
            let wantedTemplate = templates.find(template => template.id === templateId);
            let newState = {
                ...this.state,
                items:this.state.items.filter(item => !(wantedTemplate.items.includes(item))),
                templates:this.state.templates.filter(id => !(templateId === id))
            }
            this.setState(newState);
            return;
        }
    }

    isTemplateChecked = (templateId) => {
        return this.state.templates.includes(templateId);
    }

    addNewItem = () => {
        let newState = {...this.state,
                        items:[...this.state.items,this.newItem]
                       }
        this.setState(newState);
    }

    onItemsNameChange = (changedName) => {
        this.newItem.name = changedName;
    }
    

    render() {
        return (
            <React.Fragment>
                <Input 
                    placeholder="List Name"
                    onChangeText={this.onChangeText}
                />
                {
                    this.props.templates.map(template => {
                        return (
                            <CheckBox
                              key={template.id}
                              title= {template.name}
                              checked= {this.isTemplateChecked(template.id)}
                              onPress= {() => this.onCheckTemplate(template.id)}
                            />
                        )
                    })
                }
                <Card title="List Items">
                  {
                    this.state.items.map(item => {
                      return (
                        <View key={item.id}>
                          <Text h4>{item.name}</Text>
                        </View>
                      );
                    })
                  }
                    <Input 
                        placeholder="Item Name"
                        onChangeText={this.onItemsNameChange}
                    />
                    <Divider style={{backgroundColor:'white', height:10}}/>
                    <Button
                        title="Add item"
                        onPress={this.addNewItem}
                    />
                </Card>
                <Button
                    title="Save"
                    onPress={this.saveNewList}
                />
            </React.Fragment>
        )
    }
}

export default connect(mapStateToProps)(ListForm)
