import React, { Component } from 'react';
import { Provider, connect } from 'react-redux';
import { FlatList, StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Card, ListItem, Button, CheckBox, Input } from 'react-native-elements'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from './Screens/HomeScreen';
import AddListScreen from './Screens/AddListScreen';
import AddTemplateScreen from './Screens/AddTemplateScreen'
import ShowListScreen from './Screens/ShowListScreen';
import store from './store.js';
import { PersistGate } from 'redux-persist/integration/react'


const AppNavigator = createStackNavigator({
        Home: {
            screen: HomeScreen,
        },
        AddList: {
            screen: AddListScreen,
        },
        ShowList: {
            screen: ShowListScreen,
        },
        AddTemplate: {
            screen: AddTemplateScreen,
        },
    },
    {
        initialRouteName: 'Home',
    },
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component {
    renderLoading() {
        return(
            <View>
                <ActivityIndicator size={"large"} />
            </View>
        )
    }
  render() {

    return(
        <Provider store={store().store}>
            <PersistGate loading={this.renderLoading()} persistor={store().persistor}>
              <AppContainer />
            </PersistGate>
        </Provider>
    )
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 24
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  button: {
    paddingTop: 25,
    fontSize: 25,
    height: 22,
  },
})
