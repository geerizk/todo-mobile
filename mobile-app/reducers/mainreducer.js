// action types
export const types = {
  REMOVE_ITEM: "REMOVE-ITEM",
  TOGGLE_DONE_ITEM: "TOGGLE-DONE-ITEM",
  ADD_ITEM: "ADD-ITEM",
  SAVE_INPUT: "SAVE-INPUT",
  ADD_TEMPLATE: "ADD-TEMPLATE",
  ADD_LIST: "ADD-LIST",
  DELETE_ITEM: "DELETE-ITEM",
}

export const actionCreator = {
  remove: item => {
    return { type: types.REMOVE_ITEM, payload: item  }
  },
  done: item => {
    return { type: types.TOGGLE_DONE_ITEM, payload: item  }
  },
  add: string => {
    return { type: types.ADD_ITEM, payload: string  }
  },
  saveInput: string => {
    return { type: types.SAVE_INPUT, payload: string }
  },
  addTemplate: string => {
    return { type: types.ADD_TEMPLATE, payload: string }
  },
  addList: obj => {
    return { type: types.ADD_LIST, payload: obj }
  },
  addItem: obj => {
    return { type: types.ADD_ITEM, payload: obj }
  },
  deleteItem: obj => {
    return { type: types.DELETE_ITEM, payload: obj }
  },
}

// reducer
export const reducer = (state, action) => {
  if (action.type === types.REMOVE_ITEM){
    const {users} = state;
    return {
      ...state,
      users: users.filter((u, i) => u.id !== action.payload)
    }
  }
  if (action.type === types.TOGGLE_DONE_ITEM){

    return {
      ...state,
      lists: [
        ...state.lists.map((list, index) => {
          if (list.id !== action.payload.listId){
            return list
          }
          return {
            ...list,
            items: list.items.map((item, index) => {
              if (item.id !== action.payload.item.id){
                return item
              }
              return {
                ...item,
                isDone: !item.isDone
              }
            })
          }
        })
      ]
    }
  }

  if(action.type === types.ADD_TEMPLATE){
    if (typeof action.payload == "undefined")
      return state
    const {templates} = state
     return {
        ...state,
        templates: [ ...templates,  {
          name: action.payload,
          items: [],
        }]
      }
  }

  if(action.type === types.ADD_LIST){
    if (typeof action.payload == "undefined")
      return state
    const {lists} = state
     return {
        ...state,
        lists: [ ...lists,  action.payload]
      }
  }

  if(action.type === types.ADD_ITEM){
    if (typeof action.payload == "undefined")
      return state

     return {
        ...state,
        lists: state.lists.map((list, index) => {
          if (list.id !== action.payload.listId) {
            return list
          }
          return {
            ...list,
            items: [
              ...list.items,
              action.payload.item
            ]
          }
        })
      }
  }

  if(action.type === types.DELETE_ITEM){
    if (typeof action.payload == "undefined")
      return state
     return {
        ...state,
        lists: state.lists.map((list, index) => {
          if (list.id !== action.payload.listId) {
            return list
          }
          return {
            ...list,
            items: list.items.filter((item) => item.id !== action.payload.item.id)
          }
        })
      }
  }

  return state
}

export const initialState = {
      users: [
          {
              name: "Sudr",
              template: "Camping",
              isDone: false,
              id: 1,
          },
          {
              name: "Sokhna",
              template: "None",
              isDone: false,
              id: 2,
          },
          {
              name: "Sokhna",
              template: "None",
              isDone: false,
              id: 3,
          },
      ],
      templates: [
        {
          id: 1,
          name: "Test",
            items: [{
                id:1,
                name:'koko',
                isDone: false
            }]
        }
      ],
      lists: [
        {
          id: 1,
          name: "Test",
          items: []
        }
      ]
  }
