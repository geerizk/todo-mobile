import { createStore } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import { reducer } from './reducers/mainreducer.js'
import { initialState } from './reducers/mainreducer.js'
import {AsyncStorage} from 'react-native'

const persistConfig = {
  key: 'root4',
  storage: AsyncStorage,
}

const persistedReducer = persistReducer(persistConfig, reducer)

export default () => {
  let store = createStore(persistedReducer, initialState)
  let persistor = persistStore(store)
  return { store, persistor }
}
