import React, { Component } from 'react';
import { Provider, connect } from 'react-redux';
import { FlatList, StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import { Card, ListItem, Button, CheckBox, Input } from 'react-native-elements'
import { actionCreator } from './../reducers/mainreducer.js';
import ListForm from './../Containers/ListForm';


class AddListScreen extends Component {
    static navigationOptions = {
        title: "Add List..",
    }

    render() {
        return (
            <View>
                <ListForm navigation={this.props.navigation} />
            </View>
        )
    }
}

export default AddListScreen;
