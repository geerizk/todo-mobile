import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import TemplateCard from './../Containers/TemplateCard';
import ListCard from './../Containers/ListCard';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
 } from 'react-native-responsive-screen'
import Listing from './../Containers/Listing';

export default class HomeScreen extends Component {
    static navigationOptions = {
        title: "Ya Bish",
    }
    render() {
        return (
            <View style={styles.container}>
                <Listing navigation={this.props.navigation} title="Lists" showScreen="ShowList" addScreen="AddList" stateListsVariable="lists" />
                <Listing navigation={this.props.navigation} title="Templates" showScreen="ShowTemplate" addScreen="AddTemplate" stateListsVariable="templates" />
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 24
  },
  item: {
    padding: 10,
    fontSize: 18,
    width: wp('70%'),
    height: hp('30%'),
  },
  button: {
    paddingTop: 25,
    fontSize: 25,
    width: wp('70%'),
    height: hp('10%'),
  },
})
