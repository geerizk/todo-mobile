import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View } from 'react-native';
import ListCard from './../Containers/ListCard';

class ShowListScreen extends Component {
  render() {
    return(
      <ListCard
        id={this.props.navigation.getParam("id")}
      />
    )
  }
}

export default connect()(ShowListScreen)
