import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { Card, Button, CheckBox, ListItem, Input } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
import { actionCreator } from './../reducers/mainreducer.js';
import store from './../store.js';
import TodoListItem from './../Components/TodoListItem.js';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
   } from 'react-native-responsive-screen'

const mapStateToProps = (state, ownProps) => ({
  lists: state.lists,
  id: ownProps.id
})

class ListCard extends Component {

  newItemInput = {
    name: "",
    id: Math.floor(Math.random() * 1000),
    isDone: false
  }

    saveInputToState = (string) => { this.newItemInput.name = string  }

    addItem = () => {
      const {dispatch} = this.props
      dispatch(actionCreator.addItem({item: this.newItemInput, listId: this.props.id}))
    }

    render() {
        const showDone = true
        const {id} = this.props
        const {lists} = this.props
        const list = lists.find(l => l.id == id)
        const {items} = list
        // Sorts by done or not
        items.sort((a, b) => a.isDone.toString().localeCompare(b.isDone.toString()))
        return (
            <Card title={"List: "+list.name}>
                {
                    items.map((u, i) => {
                      if(!u.isDone || showDone){
                          return (
                              <View key={i}>
                                  <TodoListItem
                                      id={u.id}
                                      item={u}
                                      listId={list.id}
                                  />
                              </View>
                          );
                      }
                    })
                }
                <View key={0}>
                    <Input 
                        placeholder="Item name"
                        onChangeText={this.saveInputToState}
                    />
                    <Button
                        icon={
                            <Icon
                                name="plus"
                                size={15}
                                color="white"
                            />
                        }
                        type="solid"
                        style={styles.button}
                        onPress={() => this.addItem()}
                    />
                    <Button
                        icon={
                            <Icon
                                name="plus"
                                size={15}
                                color="blue"
                            />
                        }
                        type="solid"
                        style={styles.button}
                        onPress={() => showDone = !showDone}
                    />
                </View>
            </Card>
        )
    }
}

export default connect(mapStateToProps)(ListCard)

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 24
  },
  item: {
    padding: 10,
    fontSize: 18,
    width: wp('70%'),
    height: hp('10%'),
  },
  button: {
    paddingTop: 25,
    fontSize: 25,
    width: wp('70%'),
    height: hp('10%'),
  },
  badgeContainer:{
  },
})
