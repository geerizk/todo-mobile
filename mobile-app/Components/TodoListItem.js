import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { Card, Button, CheckBox } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
import { actionCreator } from './../reducers/mainreducer.js';
import store from './../store.js';

//const mapStateToProps = (state, ownProps) => ({
//  item: state.users[state.users.findIndex(x => x.id == ownProps.id)]
//})

class TodoListItem extends Component {

  toggleItemDone = (item) => {
    const {dispatch} = this.props
    const {listId} = this.props
    dispatch(actionCreator.done({item: item, listId: listId}))
  }

  deleteItem = (item) => {
    const {dispatch} = this.props
    const {listId} = this.props
    dispatch(actionCreator.deleteItem({item: item, listId: listId}))
  }

  render() {
    const {item} = this.props

    return(
      <CheckBox
        title={item.name.concat(" - ", item.template)}
        checkedIcon='dot-circle-o'
        uncheckedIcon='circle-o'
        checked={item.isDone}
        onPress={() => this.toggleItemDone(item)}
      />
    )
  }
}

export default connect()(TodoListItem)
//export default connect(mapStateToProps)(TodoListItem)
