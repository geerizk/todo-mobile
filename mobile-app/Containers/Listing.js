import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { Card, Button, CheckBox, ListItem } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
import { actionCreator } from './../reducers/mainreducer.js';
import store from './../store.js';

const mapStateToProps = (state, ownProps) => ({
  lists: state[ownProps.stateListsVariable]
})

class Listing extends Component {
    onShowItem = (id) => {
        this.props.navigation.navigate(this.props.showScreen, {id: id})
    }


    render() {
        const {lists} = this.props
        return (
            <Card title={this.props.title}>
                {
                    lists.map(list => {
                        return (
                            <View key={list.id}>
                                <ListItem
                                    title={list.name}
                                    badge={{value: list.items ? list.items.length : 0, textStyle:{color:'white'}}} 
                                    onPress={() => this.onShowItem(list.id)}
                                />
                            </View>
                        );
                    })
                }
                <View key={0}>
                    <Button
                        icon={
                            <Icon
                                name="plus"
                                size={15}
                                color="white"
                            />
                        }
                        type="solid"
                        style={styles.button}
                        onPress={() => this.props.navigation.navigate(this.props.addScreen)}
                    />
                </View>
            </Card>
        )
    }
}

export default connect(mapStateToProps)(Listing)

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 24
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  button: {
    paddingTop: 25,
    fontSize: 25,
    height: 22,
  },
})
