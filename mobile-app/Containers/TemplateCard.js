import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { Card, Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
   } from 'react-native-responsive-screen'

const mapStateToProps = (state) => ({
    templates: state.templates,
  })

class TemplateCard extends Component {
    render() {

        const {templates} = this.props
        return (
            <Card title="Templates">
                {   
                    templates.map((t, i) => {
                    return (
                        <View key={i}>
                        <Text style={styles.item}>{t.name}</Text>
                        </View>
                    );
                    })
                }
                <View key={0}>
                    <Button
                        icon={
                            <Icon
                                name="plus"
                                size={15}
                                color="white"
                            />
                        }
                        type="solid"
                        style={styles.button}
                        onPress={() => this.props.navigation.navigate('AddTemplate')}
                    />
                </View>
            </Card>
        )
    }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 24
  },
  item: {
    padding: 10,
    fontSize: 18,
    width: wp('70%'),
    height: hp('10%'),
  },
  button: {
    paddingTop: 25,
    fontSize: 25,
    width: wp('70%'),
    height: hp('10%'),
  },
})

export default connect(mapStateToProps)(TemplateCard)

